from django.db import models

class Coordinate(models.Model):
    longitude = models.DecimalField(max_digits=10, decimal_places=4)
    latitude = models.DecimalField(max_digits=10, decimal_places=4)
    place_name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.place_name

# Create your models here.
