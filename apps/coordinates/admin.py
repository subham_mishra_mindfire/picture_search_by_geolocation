from django.contrib import admin
from apps.coordinates.models import Coordinate

admin.site.register(Coordinate)

# Register your models here.
