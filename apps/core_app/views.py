from django.shortcuts import render
from apps.coordinates.models import Coordinate
from decouple import config
import flickrapi
from picture_search_by_geolocation.settings import FLICKR_API_KEY, FLICKR_SECRET_KEY, API_CACHE_MAXENTRIES, API_CACHE_TIMEOUT
from apps.core_app.flickr_api import flickr
import logging

logger = logging.getLogger(__name__)


def home(request):
    """
    this is the home page view. It contains search bar in which
    you can search image by entering lat& long as well as by selecting
    location from a preset list.
    It also render the resulted images to the home screen.
    """
    try:
        place_list = Coordinate.objects.all()
    except:
        logger.error("Some error occured while fetching co-ordinates from the database...plese check logs")

    
    photo_search = False
    if request.method == "POST":
        photo_search = True
        
        if 'options' in request.POST:

            place = request.POST['options']
            data = place_list.values('latitude', 'longitude').filter(place_name=place).first()

            lat = data.get('latitude', None)
            long = data.get('longitude', None)
           
            
        if 'latitude' in request.POST:
            lat = request.POST['latitude']
            long = request.POST['longitude']

        
        if 'page_no' in request.POST:
            page_no = int(request.POST['page_no'])

            lat = request.POST['latitude']
            long = request.POST['longitude']

        else:
            page_no = 1

        if lat == None or long == None:
            return render (request, 'home.html', {'error': 'unable to find data for this option',
                                                'place_list': place_list,})
        try:
            photos = dict(flickr.photos.search(accuracy=11, lat=lat, lon=long, per_page=10, page=page_no))
        except flickrapi.exceptions.FlickrError as ex:
            return render (request, 'home.html', {'error': ex,'place_list': place_list,})
    

            
        photos = photos['photos']['photo']
        photo_url_list =[]
        for photo in photos:
            url = "https://farm{}.staticflickr.com/{}/{}_{}.jpg".format(str(photo['farm']), str(photo['server']), str(photo['id']), str(photo['secret']))
            photo_url_list.append(url)
        
        return render (request, 'home.html', {
                                                'place_list': place_list,
                                                'photo_urls': photo_url_list, 
                                                'page_no':page_no,
                                                'lat': lat,
                                                'long': long,
                                                'photo_search' : photo_search,
                                            })
    
    return render (request, 'home.html', {'place_list': place_list})