from django.apps import AppConfig


class FavcoreAppConfig(AppConfig):
    name = 'core_app'
