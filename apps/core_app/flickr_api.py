from picture_search_by_geolocation.settings import FLICKR_API_KEY, FLICKR_SECRET_KEY, API_CACHE_MAXENTRIES, API_CACHE_TIMEOUT
import flickrapi
from django.core.cache import cache
import logging

logger = logging.getLogger(__name__)

try:
    flickr = flickrapi.FlickrAPI(FLICKR_API_KEY, FLICKR_SECRET_KEY, format='parsed-json', cache=True)
    flickr.cache = flickrapi.SimpleCache(timeout=API_CACHE_TIMEOUT, max_entries=API_CACHE_MAXENTRIES)
    flickr.cache = cache
except flickrapi.exceptions.FlickrError as ex:
    logger.error("Some error occured message : {}".format(ex))