from django.db import models


class FavouritePhoto(models.Model):
    photo_url = models.URLField(unique=True, max_length=200)
    user_id = models.IntegerField()

    def __str__(self):
        return self.photo_url
