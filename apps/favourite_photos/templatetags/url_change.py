from django import template

register = template.Library()


@register.filter
def url_change(value):
    """
    It will change the url of the image from
    original size to the small size
    """
    return value.replace(".jpg", "_q.jpg")