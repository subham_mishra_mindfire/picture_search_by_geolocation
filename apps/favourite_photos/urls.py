from django.conf.urls import url
from apps.favourite_photos import views



urlpatterns = [
    url(r'view/$', views.FavouritePhotoList.as_view(), name='favourite_view'),
    url(r'add/$', views.add_favourite_photos, name='add_favourite_photos'),
    url(r'remove/$', views.remove_favourite_photos, name='remove_favourite_photos'),
]