from django.shortcuts import render
from django.http import JsonResponse
from apps.favourite_photos.models import FavouritePhoto
import logging
from django.views.generic import ListView

logger = logging.getLogger(__name__)



def add_favourite_photos(request):
    """
    This view is basically for posting favourite photo url to
    the database for furthure viewing
    """

    if 'url' in request.POST:
        photo_url = request.POST['url']

        if request.POST['action'] == 'add':
            try:
                FavouritePhoto.objects.get_or_create(photo_url=photo_url, user_id=1)
            except:
                logger.error('Unable to add photo from database')


    return  JsonResponse({'url': photo_url})

def remove_favourite_photos(request):
    """
    This view is basically for removing favourite photo url from
    the database for furthure viewing
    """

    if 'url' in request.POST:
        photo_url = request.POST['url']

        if request.POST['action'] == 'remove':
            try:
                FavouritePhoto.objects.filter(photo_url=photo_url).delete()
            except:
                logger.error('Some Error occured while deleting {} from database'.format(photo_url))                

    return  JsonResponse({'url': photo_url})
    

class FavouritePhotoList(ListView):
    """
    This view is basically for rendering 
    favourite photo list to the screen
    """
    context_object_name= 'photo_urls'
    model = FavouritePhoto
    template_name = 'favourite_photos.html'