# PictureSearchByGeolocation

In this project, the user can search for photos for a particular place using a place's longitude and latitude or by selecting places from a pre-defined set of places.

## Software Requirement
1. Python 3.6 or more

## How to run the application:
1. Clone the git repo `git clone https://gitlab.com/subham_mishra_mindfire/picture_search_by_geolocation.git`

2. Run `cd picture_search_by_geolocation/picture_search_by_geolocation/`

3. Create a file with name `.env` and add django secret_key, flickr api_key and flickr secret_key (for example see .env.example file)

4. Run  `cd ../` (Need to Go back to prev directory)

5. Run `pip3 install -r requirements.txt` (this will install all required python modules)

6. Run the following commands one after another <br />
        - `python3 manage.py migrate` <br />
        - `python3 manage.py makemigrations coordinates` <br />
        - `python3 manage.py makemigrations favourite_photos` <br />
        - `python3 manage.py migrate` <br />
        - `python3 manage.py loaddata apps/coordinates/fixtures/coordinates_fixtures.json` <br />
        - `python3 manage.py createsuperuser` (Add credentials for creating a super user) <br />
        - `python3 manage.py runserver` <br />

7. Go to `localhost:8000` in browser to see the server <br />

Note : -
	Get your api key and secret from [Flickr Api](https://www.flickr.com/services/api/keys/) <br />
	Please read the Terms & Conditions carefully

## Features of the Application
1. Search for photo using latitude & longitude or by name
     ![Home Screen](https://farm66.staticflickr.com/65535/49837310062_0357265528_b.jpg)

2. See the results and add photos to favourite
   ![Result Screen](https://farm66.staticflickr.com/65535/49837310037_a7b71fd430_b.jpg)

3. See your saved favourite photos
   ![Favourite Photos](https://farm66.staticflickr.com/65535/49837838032_cc6e769598_b.jpg)

## Adding more coordinates in the backend
1. For adding more locations to the preset - <br />  
     Here we can take advantage of django admin management console <br />
            - In a new tab go to `localhost:8000/admin/coordinates/coordinate/` <br /> 
            - Enter credentials which was provided at the time of creating super user <br />
            - Click on `ADD COORDINATES` to add <br />
   ![Add coordinate sccreen](https://farm66.staticflickr.com/65535/49836576228_fa46f9d448_b.jpg)
            

